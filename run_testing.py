#! /usr/bin/env python3
import data_creation
import db_config
import argparse
#import streams
import sys

parser = argparse.ArgumentParser(description="ESDC intustry testing")
parser.add_argument("--host", metavar="localhost", type=str, default="localhost", help="DB host")
parser.add_argument("--port", metavar="1521", type=int, default=1521, help="DB host port")
parser.add_argument("--service", metavar="XE", type=str, default="XE", help="DB service name as defined in tnsnames.ora")
parser.add_argument("--user", metavar="BOB", type=str, default="infbuild", help="DB user")
parser.add_argument("--password", metavar="TOP_SECRET", type=str, default=None, help="DB password. Will prompt if not specifiec")
parser.add_argument("--delete", dest='delete', action="store_true", default=False, help="Delete all registered plans records before running")
parser.add_argument("--planid", dest='plan_id', type=int, default=1, help="Plan ID to use for new records")

args = parser.parse_args()

dbConfig = db_config.DbConfig(host=args.host, port=args.port, service_name=args.service, user=args.user, password=args.password, plan_id=args.plan_id)
db = data_creation.RegPlansDb(dbConfig)
#db.createRecords100()
#db.createRecords200()
#db.createRecords400()
#db.createRecords400toReverse()
#db.createRecords400reversals()
#db.createRecords700()

if args.delete:
    resp = input("Delete was selected. Do you want to proceed? (y/N) ")
    if resp.lower() in ["y", "yes"]:
        print("Deleting existing records")
        db.deleteRecords100()
        db.deleteRecords200()
        db.deleteRecords400()
        db.deleteRecords700()
        input("Records deleted. Press enter to continue")
    else:
        print("Negative response. Nothing will be deleted")


db.cleanSetup()
for i in range(1,6):
    try:
        print(f"\nCreating file {i}... ")
        db.resetPlanStatus()
        data_creator = getattr(db, f"createRecordsFile{i}")
        data_creator()
        input("Records inserted. Waiting for outside action... (press enter to continue to the next file)")
        print("...and done!")
    except KeyboardInterrupt:
        print("\n\nSo you've chosen the path of escape. So long. And thanks for nothing")
        sys.exit(1)        
#stream_runner = streams.Runner()
#stream_runner.runAndSaveReport("D")
