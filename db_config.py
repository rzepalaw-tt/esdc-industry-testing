import os
import getpass
import cx_Oracle

class DbConfig:
    def __init__(self, host = "localhost", port = 1521, service_name = "XE", user="infbuild", password=None, plan_id=1):
        if password is None:
            password = getpass.getpass("Enter DB password for DB user %s: " % user)
        
        self.user = user
        self.passwd = password
        self.dsn = cx_Oracle.makedsn(host, port, service_name = service_name)
        self.plan_id = plan_id
        
        print("Attempt a connection to {dsn} as user {user}".format(dsn = self.dsn, user = self.user))
