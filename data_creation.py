import cx_Oracle
import db_config
import db_insert_statements as db
import pdb
import re
import datetime
import random
import uuid
import string
import itertools
from functools import partial 

random.seed()

staging_type = dict(current=1, atEsdc=2, inTransit=3)
record_status = dict(okay=1, internalFail=2, reportingInProgress=3, reported=4, esdcProcessingFail=5)
promoter_bn = "887595528RC0001"
specimen_plan_ids = [1140001, 1140002]
transaction_type = dict(contractInformation=1, 
                        beneficiaryInformation=3,
                        subscriberInformation=4,
                        contribution=11,
                        eap=13,
                        pseContributionWithdrawal=14,
                        transferIn=19,
                        grantRepayment=21,
                        terminationAdjustment=22,
                        transferOut=23,
                        requestForClbPayment=24)
                     
grant_repayment_reason = dict(contributionWithdrawal=1,
                              aip=2,
                              contractTermination=3,
                              ineligibleTransfer=4,
                              ineligibleBenReplacement=5,
                              paymentToEducationalInstitution=6,
                              revocation=7,
                              ceasesSiblingOnly=8,
                              deceased=9,
                              overcontributionWithdrawal=10,
                              other=11,
                              nonResident=12)

def gen_record_id(starting_id):
    while True:
        yield starting_id
        starting_id = starting_id + 1

def randomDateInPeriod():
    start_date = datetime.datetime(2022, 4, 1)
    return start_date + datetime.timedelta(days=random.randrange(0,31))

def lastDayInPeriod():
    next_month = randomDateInPeriod().replace(day=28) + datetime.timedelta(days=4)
    # subtract the number of remaining 'overage' days to get last day of current month, or said programattically said, the previous day of the first of next month
    return next_month - datetime.timedelta(days=next_month.day)

def _generateTransId(length, tran_type=None):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))

def _generateOtherTransId(length, tran_type=None):
    return _generateTransId(length) if tran_type in [19] else None

def _generateContractId(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))

def _is_sin_valid(num):
    if len(num) != 9: 
        return False

    str_num = str(num)
    first_digit = str_num[0]
    if first_digit in ['0', '8']:
        return False

    checksum = 0
    for a, b in zip(str_num, '121212121'):
        res = int(a) * int(b)
        if res > 9:
            res = res % 10 + 1
        checksum = checksum + res

    return checksum % 10 == 0

_usedSins = []

def _generateSin():
    trial_sin = ''.join(random.choices(string.digits.translate(str.maketrans('','','08')), k=1)) + ''.join(random.choices(string.digits, k=8))
    while not _is_sin_valid(trial_sin) and not trial_sin in _usedSins:
        trial_sin = str(int(trial_sin) + 1)
    _usedSins.append(trial_sin)
    return trial_sin

def _generateGivenName():
    return random.choice(["Albus", "Albert", "Harry", "Ron", "Cedric", "Fred", "Brian", "Tom", "Severus"])

def _generateSurname():
    return random.choice(["Dumbledore", "Potter", "Weasley", "Riddle", "Diggory", "Snape"])

def _generateDate(min_date=datetime.date(2007, 1, 1), max_date=datetime.date.today()):
    delta = max_date - min_date
    return min_date + datetime.timedelta(days=random.randrange(delta.days)) 

def _generateCountry():
    # make CAN addresses more likely than others, arbitrary choice of weights
    return random.choices(["CAN", "USA", "OTH"], weights=[15, 4, 1], k=1)[0]

def _generateAddress1(country = "CAN"):
    return "Address1"

def _generateAddress2(country = "CAN"):
    province_equiv = ""
    if country != "CAN":
        province_equiv = _generateProvince(country)
    return province_equiv

def _generateAddress3(country = "CAN"):
    if country == "OTH":
        country = random.choice(["Germany", "Mexico", "Bermuda"])
    return country or " "

def _generatePostalCode(country = "CAN"):
    postal_code = ""
    if country == "CAN":
        chars = [string.ascii_uppercase,
                 string.digits,
                 string.ascii_uppercase,
                 [" "],
                 string.digits,
                 string.ascii_uppercase,
                 string.digits]
        for char_type in chars:
            postal_code = postal_code + random.choice(char_type)

    return postal_code if len(postal_code) > 0 else "  " #otherwise cx_Oracle tried a NULL 

def _generateProvince(country = "CAN"):
    province = "  "
    if country == "CAN":
        province = random.choice(["AB", "BC", "MB", "NB", "NL", "NS", "NT", "NU", "ON", "PE", "QC", "SK", "YT"])
    elif country == "USA":
        province = random.choice(["NY", "NJ", "CT", "MA", "RI", "VT", "NH"])

    return province

def _generateCity():
    return random.choice(["City1", "City 2", "City-3"])

def _getContributionAmount(tran_type, min_val=0, max_val=9_999_999):
    if tran_type in [ transaction_type['contribution'], transaction_type['requestForClbPayment'] ]:
        return random.uniform(min_val, max_val)
    return 0 #TODO this should be nullable

def _getGrantRequestedInd(tran_type):
    if tran_type in [ transaction_type['contribution'], transaction_type['requestForClbPayment'] ]:
        return random.choice([0,1])
    return 0 #TODO should be nullable

def _getAcademicYearStartDate(tran_type):
    if tran_type in [ transaction_type['eap'], transaction_type['pseContributionWithdrawal'] ]:
        return datetime.date(2021, 10, 1)
    return datetime.date(2021, 10, 1)
    #return None #TODO this should be nullalble

def _getAcademicYearLength(tran_type):
    if tran_type in [ transaction_type['eap'], transaction_type['pseContributionWithdrawal'] ]:
        return random.choice([30, 40, 50])
    return 0 # TODO this shoudl be nullable
    return None #TODO this should be nullalble

def _getAcademicInstPostalCode(tran_type):
    if tran_type in [ transaction_type['eap'], transaction_type['pseContributionWithdrawal'] ]:
        return _generatePostalCode()
    return " " #TODO this should be nullalble
    
def _getGrantAmount(tran_type, min_val=0, max_val=9_999_999):
    if tran_type in [ transaction_type['transferIn'],
                      transaction_type['grantRepayment'],
                      transaction_type['terminationAdjustment'],
                      transaction_type['transferOut'] ]:
        return random.uniform(min_val, max_val)
    return 0 #TODO should be nullable

def _getEapGrantAmount(tran_type, min_val=0, max_val=9_999_999):
    if tran_type in [ transaction_type['eap'] ]:
        return random.uniform(min_val, max_val)
    return 0 #TODO should be nullable

def _getEapAmount(tran_type, min_val=0, max_val=9_999_999):
    if tran_type in [ transaction_type['eap'] ]:
        return random.uniform(0, 5_000)
    return 0 #TODO should be nullable

def _getPseAmount(tran_type, min_val=0, max_val=9_999_999):
    if tran_type in [ transaction_type['pseContributionWithdrawal'] ]:
        return random.uniform(min_val, max_val)
    return 0 #TODO should be nullable

def _getPseProgramLength(tran_type):
    if tran_type in [ transaction_type['eap'], transaction_type['pseContributionWithdrawal'] ]:
        return random.randint(2,4)
    return 0; #TODO should be nullable

def _getPseProgramtype(tran_type): #TODO
    if tran_type in [ transaction_type['eap'], transaction_type['pseContributionWithdrawal'] ]:
        return random.randint(1,4)
    return 0 #TODO should be nullable

def _getPseProgramYear(tran_type):
    if tran_type in [ transaction_type['eap'], transaction_type['pseContributionWithdrawal'] ]:
        return random.randint(1,4)
    return 0 #TODO should be nullable

def _getRepaymentReason(tran_type):
    if tran_type in [ transaction_type['grantRepayment'] ]:
        return random.randint(1,12)
    return 0 #TODO should be nullable

def _getPcgSpouse(tran_type):
    return _generateSin()

def _getPcgSpouseGivenName(tran_type):
    if tran_type in [ transaction_type['requestForClbPayment'] ]:
        return _generateGivenName()
    return ' ' #todo

def _getPcgSpouseSurname(tran_type):
    if tran_type in [ transaction_type['requestForClbPayment'] ]:
        return _generateSurname()
    return ' ' #todo

def _getPcgSpouseType(tran_type):
    return 1 #random.randint(1,2)

def _getClbAmount(tran_type, repayment_reason=None):
    if tran_type == 21 and repayment_reason in [1]: 
        return 0
    min_amount = 0
    max_amount = 9_999_999

    if tran_type == 12: max_amount = 5_000
    if tran_type == 19: max_amount = 20_000
    if tran_type == 21: max_amount = 5_000
    if tran_type == 22: max_amount = 20_000
    if tran_type == 23: max_amount = 20_000
    return random.uniform(min_amount, max_amount)

def _getClbEapAmount(tran_type):
    min_amount = 0
    max_amount = 9_999_999
    if tran_type == 13: max_amount = 5_000
    return random.uniform(0, max_amount)

def _getOtherSpecimenPlanId(current_specimen_plan_id, tran_type):
    if tran_type in [19, 23]:
        if current_specimen_plan_id == specimen_plan_ids[0]: return specimen_plan_ids[1]
        if current_specimen_plan_id == specimen_plan_ids[1]: return specimen_plan_ids[0]
    return None


class RegPlansDb:
    def __init__(self, config):
        self.pool = cx_Oracle.SessionPool(user=config.user, 
                                          password=config.passwd, 
                                          dsn=config.dsn,
                                          min = 1, 
                                          max = 5, 
                                          increment = 1)

        # transaction_type['record_type'],: {insert_query, callable_generator}
        self.config = {'100':            dict(insert_query = db.insert_record_100_query, generator = self._getRecordData100),
                       '200':            dict(insert_query = db.insert_record_200_query, generator = self._getRecordData200),
                       '400-file2':      dict(insert_query = db.insert_record_400_query, generator = partial(self._getRecordData400,
                                                                                                             transaction_types=[transaction_type['contribution'],
                                                                                                                                transaction_type['requestForClbPayment']])),
                       '400-file3':      dict(insert_query = db.insert_record_400_query, generator = partial(self._getRecordData400,
                                                                                                             transaction_types=[transaction_type['eap'],
                                                                                                                                transaction_type['pseContributionWithdrawal'],
                                                                                                                                transaction_type['transferIn'],
                                                                                                                                transaction_type['grantRepayment'],
                                                                                                                                transaction_type['terminationAdjustment'],
                                                                                                                                transaction_type['transferOut'],
                                                                                                                                ])),
                       '400-file2_rev':  dict(insert_query = db.insert_record_400_query, generator = partial(self._getRecordData400,
                                                                                                             transaction_types=[transaction_type['contribution'],
                                                                                                                                transaction_type['requestForClbPayment']])),
                       '400-file3_rev':  dict(insert_query = db.insert_record_400_query, generator = partial(self._getRecordData400,
                                                                                                             transaction_types=[transaction_type['eap'],
                                                                                                                                transaction_type['pseContributionWithdrawal'],
                                                                                                                                transaction_type['transferIn'],
                                                                                                                                transaction_type['grantRepayment'],
                                                                                                                                transaction_type['terminationAdjustment'],
                                                                                                                                transaction_type['transferOut'],
                                                                                                                                ])),
                       '400-reversed':   dict(insert_query = db.insert_record_400_query, generator=self._getRecordData400reversals),
                       '700':            dict(insert_query = db.insert_record_700_query, generator=self._getRecordData700),
                        # the following two are for testing 
                       '400':            dict(insert_query = db.insert_record_400_query, generator = self._getRecordData400)}

        self.generated_data = dict()
        self.plan_id = config.plan_id
                                          
    
    @staticmethod
    def _getMaxRecordIdQuery(record_type_id):
        return f"SELECT MAX(RECORD_ID) FROM REGISTERED_PLANS.RP_ESDCPL_RECORD_{record_type_id}"

    @staticmethod
    def _getDeleteQuery(record_type_id):
        return f"DELETE FROM REGISTERED_PLANS.RP_ESDCPL_RECORD_{record_type_id}"

    def _getRecordData100(self, starting_record_id, count = 100):
        assert count >= 100, "For industry testing you need at least 100 records of type 100"

        data = []
        for record_id in range(starting_record_id, starting_record_id + count):
            individual_only_ind = random.choices([0,1], weights=[1,4])[0]    #make individual more likely
            specimen_plan_id = specimen_plan_ids[individual_only_ind]
            data.append(dict(
                     record_id = record_id,
                     staging_type_id = staging_type["inTransit"],
                     record_status_id = record_status["okay"],
                     filing_report_id = None,   #not reported yet
                     transaction_date = randomDateInPeriod(),
                     promoter_trans_id = _generateTransId(random.randint(10,15)),
                     promoter_bn = promoter_bn,
                     transaction_type = transaction_type["contractInformation"],
                     specimen_plan_id = specimen_plan_id,
                     contract_id = _generateContractId(15),
                     individual_only_ind = individual_only_ind))

        return data

    def _getRecordData200(self, starting_record_id):
        contract_info_100 = self.generated_data['100']
        assert len(contract_info_100) >= 100, "For industry testing you need at least 100 records of type 100"
        record_id = gen_record_id(starting_record_id)

        data_sub = []
        data_ben = []
        for contract in contract_info_100:
            country = _generateCountry()

            #for each contract generate at least one subscriber
            for i in range(random.choices([1,2,3], weights=[12, 7, 7])[0]): # random choice of weights for subsriber nubmers
                data_sub.append(dict(
                         record_id = next(record_id),
                         staging_type_id = staging_type["inTransit"],
                         record_status_id = record_status["okay"],
                         filing_report_id = None,   #not reported yet
                         transaction_date = randomDateInPeriod(),
                         promoter_trans_id = _generateTransId(random.randrange(10,16)),
                         promoter_bn = promoter_bn,
                         transaction_type = transaction_type["subscriberInformation"],
                         specimen_plan_id = contract['specimen_plan_id'],
                         contract_id = contract['contract_id'],
                         sin = _generateSin(),
                         given_name = _generateGivenName(),
                         surname = _generateSurname(),
                         dob = _generateDate(),
                         gender = random.choice([1,2]),
                         relationship_type = random.randrange(1,8),
                         address1 = _generateAddress1(country),
                         address2 = _generateAddress2(country),
                         address3 = _generateAddress3(country),
                         city = _generateCity(),
                         province = _generateProvince(country),
                         country = country,
                         postal_code = _generatePostalCode(country),
                         language = random.choice([1,2]),
                         custodial_parent_name = _generateGivenName() + " " + _generateSurname(),
                         business_name_raw = "12",
                         business_num_raw = "11"))

            # one beneficiary on an individual contract, random number (2 or 3) on a family contract
            beneficiary_count = 1 if contract['individual_only_ind'] == 1 else random.randint(2,3)
            for i in range(beneficiary_count):
                data_ben.append(dict(
                         record_id = next(record_id),
                         staging_type_id = staging_type["inTransit"],
                         record_status_id = record_status["okay"],
                         filing_report_id = None,   #not reported yet
                         transaction_date = randomDateInPeriod(),
                         promoter_trans_id = _generateTransId(random.randrange(10,16)),
                         promoter_bn = promoter_bn,
                         transaction_type = transaction_type["beneficiaryInformation"],
                         specimen_plan_id = contract['specimen_plan_id'],
                         contract_id = contract['contract_id'],
                         sin = _generateSin(),
                         given_name = _generateGivenName(),
                         surname = _generateSurname(),
                         dob = _generateDate(),
                         gender = random.choice([1,2]),
                         relationship_type = random.randrange(1,8),
                         address1 = _generateAddress1(country),
                         address2 = _generateAddress2(country),
                         address3 = _generateAddress3(country),
                         city = _generateCity(),
                         province = _generateProvince(country),
                         country = country,
                         postal_code = _generatePostalCode(country),
                         language = random.choice([1,2]),
                         custodial_parent_name = _generateGivenName() + " " + _generateSurname(),
                         business_name_raw = "12",
                         business_num_raw = "11"))

        assert len(data_sub) >= 100, "For industry testing you need at least 100 records of type 200 for subscribers"
        assert len(data_ben) >= 100, "For industry testing you need at least 100 records of type 200 for beneficiaries"

        return data_sub + data_ben

    @staticmethod
    def _generateRecord400(record_id, tran_type, info, repayment_reason):
        record = dict(record_id = record_id,
                      staging_type_id = staging_type["inTransit"],
                      record_status_id = record_status["okay"],
                      filing_report_id = None,   #not reported yet
                      transaction_date = randomDateInPeriod(),
                      promoter_trans_id = _generateTransId(random.randrange(10,16)),
                      promoter_bn = promoter_bn,
                      transaction_type = tran_type, 
                      specimen_plan_id = info['specimen_plan_id'], 
                      contract_id = info['contract_id'],
                      subscriber_sin = info['subscriber_sin'],
                      beneficiary_sin = info['beneficiary_sin'],

                      contribution_amount = _getContributionAmount(tran_type), # should be nullable
                      grant_requested_ind = _getGrantRequestedInd(tran_type),
                      academic_year_start_date = _getAcademicYearStartDate(tran_type), # should be nullable
                      academic_year_length = _getAcademicYearLength(tran_type), # should be nullable

                      #reversal block  
                      reversal_flag_ind = 1, #normal, no reversal
                      orig_promoter_trans_id = None, 
                      orig_promoter_bn = None, 

                      grant_amount = _getGrantAmount(tran_type), # should be nullable
                      eap_grant_amount = _getEapGrantAmount(tran_type), # should be nullable
                      eap_amount = _getEapAmount(tran_type), # should be nullable
                      pse_amount = _getPseAmount(tran_type), # should be nullable

                      #transfers
                      other_specimen_plan_id = _getOtherSpecimenPlanId(info['specimen_plan_id'], tran_type),
                      other_contract_id = None, #TODO this is wrong

                      repayment_reason = repayment_reason, # should be nullable

                      pse_program_length = _getPseProgramLength(tran_type), # should be nullable
                      pse_program_type = _getPseProgramtype(tran_type), # should be nullable
                      pse_program_year = _getPseProgramYear(tran_type), # should be nullable
                      edu_institution_postal_code = _getAcademicInstPostalCode(tran_type), # should be nullable

                      pcg_spouse = _getPcgSpouse(tran_type),
                      pcg_spouse_given_name = _getPcgSpouseGivenName(tran_type),
                      pcg_spouse_surname = _getPcgSpouseSurname(tran_type),
                      pcg_spouse_type = _getPcgSpouseType(tran_type),

                      clb_amount = _getClbAmount(tran_type, repayment_reason),
                      clb_eap_amount = _getClbEapAmount(tran_type),
                      sages_amount = 0,
                      sages_eap_amount = 0,
                      bctesg_amount = 0,
                      bctesg_eap_amount = 0)
        return record

    def _getRecordData400(self, starting_record_id, transaction_types=[transaction_type['contribution']]):
        contract_info_100 = self.generated_data['100']
        benesub_info_200 = self.generated_data['200']
        transaction_types = transaction_types * 15 #minimum required multiplicity is 15, will be an array [a, b, c, a, b, c], as oposed to [a, a, b, b, c, c]

        plan_info = []
        for beneficiary in (ben_info for ben_info in benesub_info_200 if ben_info['transaction_type'] == transaction_type["beneficiaryInformation"]):
            for subscriber in (sub_info for sub_info in benesub_info_200 if sub_info['transaction_type'] == transaction_type["subscriberInformation"] and sub_info['contract_id'] == beneficiary['contract_id']):
                plan_info.append(dict(contract_id = beneficiary['contract_id'],
                                      specimen_plan_id = beneficiary['specimen_plan_id'],
                                      beneficiary_sin = beneficiary['sin'],
                                      subscriber_sin = subscriber['sin']))
        
        plan_info_generator = itertools.cycle(plan_info)
        
        record_id = gen_record_id(starting_record_id)

        data = []
        for tran_type in transaction_types:
            if tran_type == transaction_type['grantRepayment']:
                for repayment_reason in grant_repayment_reason.values():
                    info = next(plan_info_generator)
                    record = self._generateRecord400(next(record_id), tran_type, info, repayment_reason)
                    data.append(record)
            else:
                info = next(plan_info_generator)
                repayment_reason = 0
                record = self._generateRecord400(next(record_id), tran_type, info, repayment_reason)
                data.append(record)
        return data
                
    def _getRecordData400reversals(self, starting_record_id):
        original_transactions = self.generated_data['400-file2_rev'] + self.generated_data['400-file3_rev']

        record_id = gen_record_id(starting_record_id)
        data = []

        for original in original_transactions:
            copy = original.copy()
            copy['transaction_date'] = randomDateInPeriod()
            copy['record_id'] = next(record_id)
            copy['promoter_trans_id'] = _generateTransId(random.randrange(10,16))
            copy['reversal_flag_ind'] = 1
            copy['orig_promoter_trans_id'] = original['promoter_trans_id']
            copy['orig_promoter_bn'] = original['promoter_bn']
            data.append(copy)

        return data


    def _getRecordData700(self, starting_record_id):
        contract_info_100 = self.generated_data['100']
        assert len(contract_info_100) >= 100, "For industry testing you need at least 100 records of type 100"
        record_id = gen_record_id(starting_record_id)

        data = []
        
        for contract in contract_info_100:
            data.append(dict(record_id = next(record_id),
                             staging_type_id = staging_type["inTransit"],
                             record_status_id = record_status["okay"],
                             filing_report_id = None,   #not reported yet
                             transaction_date = lastDayInPeriod(),
                             promoter_bn = contract['promoter_bn'],
                             specimen_plan_id = contract['specimen_plan_id'], 
                             contract_id = contract['contract_id'],
                             total_resp_assets = random.uniform(0, 9_999_999)))

        return data

    def _createRecordsForType(self, record_type, **kwargs):
        connection = self.pool.acquire()
        cursor = connection.cursor()

        #rec_type = re.sub("-.*", "", record_type)
        #cursor.execute(self._getMaxRecordIdQuery(rec_type))
        #result = cursor.fetchall()
        #if len(result) != 1:
        #    raise f"Too many results for record type {record_type}!"

        #next_id = result[0][0] + 1 if result[0][0] != None else 1 # next id if rows exist (!=None) or start afresh
        cursor.execute(f"SELECT MAX(RECORD_ID) FROM REGISTERED_PLANS.RP_ESDCPL_PLAN_RECORD")
        result = cursor.fetchall()

        next_id = result[0][0] + 1 if result[0][0] != None else 1 # next id if rows exist (!=None) or start afresh
        rec_id_generator = gen_record_id(next_id)


        data=self.config[record_type]['generator'](next_id)
        self.generated_data[record_type] = data #save for good measure
        try:
            cursor.executemany(self.config[record_type]['insert_query'], data, batcherrors=True)
        except cx_Oracle.DatabaseError as err:
            print(cursor.statement)
            pdb.set_trace()
            raise

        print(f"Inserted {cursor.rowcount} new records of type {record_type}")
        for error in cursor.getbatcherrors():
            print("Error", error.message, "at row offset", error.offset)

        connection.commit()

    def _linkRecordsToPlan(self, generated_data_id):
        plan_id=self.plan_id
        type_id = re.sub("-.*", "", generated_data_id)
        records = self.generated_data[generated_data_id]

        query = f"""INSERT INTO REGISTERED_PLANS.RP_ESDCPL_PLAN_RECORD (RECORD_ID,
                                                                        PLAN_ID,
                                                                        RECORD_TYPE_ID,
                                                                        EXTERNAL_REF,
                                                                        MODIFIED_DATE_TIME)
             VALUES (:record_id,
                     {plan_id},
                     {type_id},
                     :external_ref,
                     :mod_time)
        """
        
        connection = self.pool.acquire()
        cursor = connection.cursor()
        cursor.execute(f"SELECT MAX(RECORD_ID) FROM REGISTERED_PLANS.RP_ESDCPL_PLAN_RECORD")
        result = cursor.fetchall()
        if len(result) != 1:
            raise f"Too many results for record type {record_type}!"

        next_id = result[0][0] + 1 if result[0][0] != None else 1 # next id if rows exist (!=None) or start afresh
        rec_id_generator = gen_record_id(next_id)

        data = []
        for record in records:
            data.append(dict(record_id = record['record_id'],
                             external_ref = record.get('promoter_trans_id') or str(uuid.uuid1()),
                             mod_time = datetime.datetime.now()))
        

        cursor.executemany(query, data, batcherrors=True)
        for error in cursor.getbatcherrors():
            print("Error", error.message, "at row offset", error.offset)
        connection.commit()
            
    def _deleteRecordsForType(self, type_id):
        connection = self.pool.acquire()
        cursor = connection.cursor()
        
        cursor.execute(self._getDeleteQuery(type_id))        

        connection.commit()

    def createRecords100(self):
        return self._createRecordsForType('100')

    def createRecords200(self):
        return self._createRecordsForType('200')

    def createRecords400(self):
        return self._createRecordsForType('400')

    def createRecords400toReverse(self):
        return self._createRecordsForType('400-to_reverse')

    def createRecords400reversals(self):
        return self._createRecordsForType('400-reversed')

    def createRecords700(self):
        return self._createRecordsForType('700')

    def createRecordsFile0(self):
        print("File 0 is a temporary workaround until a db bug is fixed. Records 100 and 200 should go into file 1")
        self._createRecordsForType('100')
        self._linkRecordsToPlan('100')


    def createRecordsFile1(self):
        self._createRecordsForType('100')
        self._linkRecordsToPlan('100')
        
        self._createRecordsForType('200')
        self._linkRecordsToPlan('200')

    def createRecordsFile2(self):
        self._createRecordsForType('400-file2')
        self._linkRecordsToPlan('400-file2')

        self._createRecordsForType('400-file2_rev')
        self._linkRecordsToPlan('400-file2_rev')

    def createRecordsFile3(self):
        self._createRecordsForType('400-file3')
        self._linkRecordsToPlan('400-file3')

        self._createRecordsForType('400-file3_rev')
        self._linkRecordsToPlan('400-file3_rev')

    def createRecordsFile4(self):
        self._createRecordsForType('400-reversed')
        self._linkRecordsToPlan('400-reversed')

    def createRecordsFile5(self):
        self._createRecordsForType('700')
        self._linkRecordsToPlan('700')

    def deleteRecords100(self):
        self._deleteRecordsForType('100')

    def deleteRecords200(self):
        self._deleteRecordsForType('200')

    def deleteRecords400(self):
        self._deleteRecordsForType('400')

    def deleteRecords700(self):
        self._deleteRecordsForType('700')

    def resetPlanStatus(self):
        connection = self.pool.acquire()
        cursor = connection.cursor()
        
        cursor.execute("UPDATE REGISTERED_PLANS.RP_ESDCPL_PLAN SET PLAN_STATUS_ID = 3")
        cursor.execute("""DELETE FROM REGISTERED_PLANS.RP_ESDCPL_PLAN_FILE_MAP""")

        connection.commit()
        
    def cleanSetup(self):
        connection = self.pool.acquire()
        cursor = connection.cursor()
        
        plan_id=self.plan_id
        print(f"Removing all plans and readding plan ID {plan_id}")
        #remove all plans and add a plan in a staging state
        cursor.execute(""" DELETE FROM REGISTERED_PLANS.RP_ESDCPL_PLAN """)
        cursor.execute(f"""
        INSERT INTO REGISTERED_PLANS.RP_ESDCPL_PLAN (PLAN_ID,
                                                     SEQUENCE_NUM,
                                                     PLAN_STATUS_ID,
                                                     EXTERNAL_REF,
                                                     MODIFIED_DATE_TIME,
                                                     PLAN_START_DATE)
             VALUES ({plan_id},
                     0,
                     3,
                     'TEST',
                     SYSDATE,
                     TO_DATE ('01/04/22', 'MM/DD/YY'))
        """)

        
        print(f"Delete all plan statuses and insert plan status staging for plan ID {plan_id}")
        cursor.execute("""DELETE FROM REGISTERED_PLANS.RP_ESDCPL_PLAN_STATUS""")
        cursor.execute(f"""
        INSERT INTO REGISTERED_PLANS.RP_ESDCPL_PLAN_STATUS (PLAN_ID,
                                                            STATUS_INDEX,
                                                            STATUS_TYPE_ID,
                                                            IS_LATEST_IND,
                                                            NARRATIVE,
                                                            ENTRY_DATE_TIME,
                                                            STATUS_DATE_TIME)
             VALUES ({plan_id},
                     1,
                     3,
                     'T',
                     'TEST',
                     SYSDATE,
                     SYSDATE)
        """)

        # no idea why, probably a bug but it crashes the process
        cursor.execute("""DELETE FROM REGISTERED_PLANS.RP_ESDCPL_PLAN_FILE_MAP""")
        
        cursor.execute("""DELETE FROM REGISTERED_PLANS.RP_ESDCPL_RECORD_100""")
        cursor.execute("""DELETE FROM REGISTERED_PLANS.RP_ESDCPL_PLAN_RECORD""")
        connection.commit()
        

__all__ = [ 'RegPlansDb' ]
