#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# (c) 2022 Torstone Technology Limited, all rights reserved
#
# __author__ = Wojtek Rzepala
#

import sys
import os
import argparse
import re

parser = argparse.ArgumentParser(description="ESDC report formatter. Provide a file name or read from standard input")
parser.add_argument("input", nargs="?", metavar='file', type=str, default=None, help="Input source. Defaults to stdin")
parser.add_argument("-s", "--separator", dest='separator', type=str, default="|", help="Column separator")
parser.add_argument("--show-headers", dest='show_headers', action="store_true", default=False, help="Show column headers at the beginning of each section. Column name includes column index in the current output (fillers taken into account) and character index of the column data in the original file. Both starting with index 1. This is meant bo be used with tools like cut")
parser.add_argument("--show-section-names", dest='show_section_names', action="store_true", default=False, help="Show section name at the beginning of each section")
parser.add_argument("--skip-filler", dest='skip_filler', action="store_true", default=False, help="Skip filler columns in output")

args = parser.parse_args()

infile  = sys.stdin
if args.input != None:
    infile = open(args.input, 'r')
    filename_pattern = re.compile(r"(?P<type>[PTSZ]{1})(?P<BN>\w{15})(?P<date>\d{8})(?P<filenum>\d{2})\.?(?P<ext>\w{3})?")
    required_filename = filename_pattern.match(os.path.basename(infile.name))
    if required_filename:
        filetype = ""
        if required_filename.group("ext"):
            types = dict( pro = "Processing report from the Program to Sender",
                          err = "Error report from the Program to Sender",
                          reg = "Contract registration report from the Program to Sender",
                          ser = "Severe error report from the Program to Sender",
                          svr = "SIN validation report from the Program to the Sender",
                          ref = "Referral report from the Program to the Promoter" )
            env = "Production" if required_filename.group("type") in ["P", "S"] else "Test"

            filetype = env + " " + types[required_filename.group("ext")]
        else:
            matched_type = required_filename.group("type")
            types = dict( P = "Production report submission",
                          T = "Test report submission",
                          S = "Production summary file",
                          Z = "Test summary file" )
            filetype = types[matched_type]
            
        print("""Processing file {name}\n\nFile type:\t{type}\nBussiness Num:\t{bn}\nDate: \t\t{date}\nFile number:\t{number}\n""".format(name = infile.name,
                                                                                                                                          type = filetype,
                                                                                                                                          bn = required_filename.group("BN"),
                                                                                                                                          date = required_filename.group("date"),
                                                                                                                                          number = required_filename.group("filenum")))
outfile = sys.stdout
separator = args.separator 

# column indices to appear as they do in the ITS doc
config = {
    '001' : dict(columns = [dict(index = [  1,   3], name = "Record Type"),
                            dict(index = [  4,  18], name = "Sender BN"),
                            dict(index = [ 19,  26], name = "Date Sent"),
                            dict(index = [ 27,  28], name = "File Number"),
                            dict(index = [ 29,  31], name = "Data Version"),
                            dict(index = [ 32, 500], name = "Filler", isFiller = True)],
                 name = "Header Record"),
    '002' : dict(columns = [dict(index = [  1,   3], name = "Record Type"),
                            dict(index = [  4,  18], name = "Promoter BN"),
                            dict(index = [ 19,  26], name = "Start Date Of Reporting Period"),
                            dict(index = [ 27,  34], name = "End Date Of Reporting Period"),
                            dict(index = [ 35,  46], name = "Summary Amount"),
                            dict(index = [ 27,  58], name = "Payment Amount"),
                            dict(index = [ 59,  68], name = "Payment Requisition ID"),
                            dict(index = [ 69, 500], name = "Filler", isFiller = True)],
                 name = "Sub-Header Record"),
    '003' : dict(columns = [dict(index = [  1,   3], name = "Record Type"),
                            dict(index = [  4,  18], name = "Sender BN"),
                            dict(index = [ 19,  26], name = "Date Sent"),
                            dict(index = [ 27,  28], name = "File Number"),
                            dict(index = [ 29, 500], name = "Filler", isFiller = True)],
                 name = "Sub-Header Record"),
    '100' : dict(columns = [dict(index = [  1,   3], name = "Record Type"),
                            dict(index = [  4,  11], name = "Transaction Date"),
                            dict(index = [ 12,  26], name = "Promoter Transaction ID"),
                            dict(index = [ 27,  41], name = "Promoter BN"),
                            dict(index = [ 42,  43], name = "Transaction Type"),
                            dict(index = [ 44,  53], name = "Specimen Plan ID"),
                            dict(index = [ 54,  68], name = "Contract ID"),
                            dict(index = [ 69, 102], name = "Filler", isFiller = True),
                            dict(index = [103, 103], name = "Individual/Sibling Only"),
                            dict(index = [104, 500], name = "Filler", isFiller = True)],
                 name = "Contract Information"),
    '200' : dict(columns = [dict(index = [  1,   3], name = "Record Type"),
                            dict(index = [  4,  11], name = "Transaction Date"),
                            dict(index = [ 12,  26], name = "Promoter Transaction ID"),
                            dict(index = [ 27,  41], name = "Promoter BN"),
                            dict(index = [ 42,  43], name = "Transaction Type"),
                            dict(index = [ 44,  53], name = "Specimen Plan ID"),
                            dict(index = [ 54,  68], name = "Contract ID"),
                            dict(index = [ 69,  77], name = "SIN"),
                            dict(index = [ 78,  97], name = "Given Name"),
                            dict(index = [ 98, 117], name = "Surname"),
                            dict(index = [118, 125], name = "Birth Date"),
                            dict(index = [126, 126], name = "Sex"),
                            dict(index = [126, 126], name = "Sex"),
                            dict(index = [127, 127], name = "Relationship Type"),
                            dict(index = [128, 167], name = "Address Line 1"),
                            dict(index = [168, 207], name = "Address Line 2"),
                            dict(index = [208, 247], name = "Address Line 3"),
                            dict(index = [248, 277], name = "City"),
                            dict(index = [278, 279], name = "Province"),
                            dict(index = [280, 282], name = "Country"),
                            dict(index = [283, 292], name = "Postal Code"),
                            dict(index = [293, 409], name = "Filler", isFiller = True),
                            dict(index = [410, 410], name = "Language"),
                            dict(index = [411, 440], name = "Custodial Parent Name"),
                            dict(index = [441, 500], name = "Filler", isFiller = True)],
                 name = "Beneficiary / Subscriber Information"),
    '400' : dict(columns = [dict(index = [  1,   3], name = "Record Type"),
                            dict(index = [  4,  11], name = "Transaction Date"),
                            dict(index = [ 12,  26], name = "Promoter Transaction ID"),
                            dict(index = [ 27,  41], name = "Promoter BN"),
                            dict(index = [ 42,  43], name = "Transaction Type"),
                            dict(index = [ 44,  53], name = "Specimen Plan ID"),
                            dict(index = [ 54,  68], name = "Contract ID"),
                            dict(index = [ 69,  77], name = "Subscriber SIN"),
                            dict(index = [ 78,  86], name = "Beneficiary SIN"),
                            dict(index = [ 87,  95], name = "Contribution Amount"),
                            dict(index = [ 96,  96], name = "Grant Requested"),
                            dict(index = [ 97, 100], name = "Filler", isFiller = True),
                            dict(index = [101, 108], name = "Academic Year Start Date"),
                            dict(index = [109, 111], name = "Academic Year Length"),
                            dict(index = [112, 120], name = "Filler", isFiller = True),
                            dict(index = [121, 121], name = "Reversal Flag"),
                            dict(index = [122, 136], name = "Original Promoter Transaction ID"),
                            dict(index = [137, 151], name = "Original Promoter BN"),
                            dict(index = [152, 160], name = "Grant Amount"),
                            dict(index = [161, 169], name = "EAP Grant Amount"),
                            dict(index = [170, 178], name = "EAP Amount"),
                            dict(index = [179, 187], name = "PSE Amount"),
                            dict(index = [188, 197], name = "Other Specimen Plan ID"),
                            dict(index = [198, 212], name = "Other Contract ID"),
                            dict(index = [213, 214], name = "Repayment Reason"),
                            dict(index = [215, 215], name = "PSE Program Length"),
                            dict(index = [216, 217], name = "PSE Program Type"),
                            dict(index = [218, 227], name = "Educational Institution Postal Code"),
                            dict(index = [228, 228], name = "PSE Program Year"),
                            dict(index = [229, 243], name = "PCG/Spouse"),
                            dict(index = [244, 263], name = "PCG/Spouse Given Name"),
                            dict(index = [264, 283], name = "PCG/Spouse Surname"),
                            dict(index = [284, 284], name = "PCG/Spouse Type"),
                            dict(index = [285, 293], name = "CLB Amount"),
                            dict(index = [294, 302], name = "CLB EAP Amount"),
                            dict(index = [303, 322], name = "Filler", isFiller = True),
                            dict(index = [323, 331], name = "SAGES Amount"),
                            dict(index = [332, 340], name = "SAGES EAP Amount"),
                            dict(index = [341, 349], name = "BCTESG Amount"),
                            dict(index = [350, 358], name = "BCTESG EAP Amount"),
                            dict(index = [359, 500], name = "Filler", isFiller = True)],
                 name = "Beneficiary / Subscriber Information"),
    '511' : dict(columns = [dict(index = [  1,   3], name = "Record Type"),
                            dict(index = [  4,  11], name = "Transaction Date"),
                            dict(index = [ 12,  26], name = "Promoter Transaction ID"),
                            dict(index = [ 27,  41], name = "Promoter BN"),
                            dict(index = [ 42,  43], name = "Transaction Type"),
                            dict(index = [ 44,  53], name = "Specimen Plan ID"),
                            dict(index = [ 54,  68], name = "Contract ID"),
                            dict(index = [ 69,  83], name = "Contribution Promoter Transaction ID"),
                            dict(index = [ 84,  98], name = "Contribution Promoter BN"),
                            dict(index = [ 99, 113], name = "PCG/Spouse"),
                            dict(index = [114, 133], name = "PCG/Spouse Given Name"),
                            dict(index = [134, 153], name = "PCG/Spouse Surname"),
                            dict(index = [154, 154], name = "PCG/Spouse Type"),
                            dict(index = [155, 500], name = "Filler", isFiller = True)],
                 name = "PCG/Spouse Information"),
    '700' : dict(columns = [dict(index = [  1,   3], name = "Record Type"),
                            dict(index = [  4,  11], name = "Transaction Date"),
                            dict(index = [ 12,  26], name = "Promoter BN"),
                            dict(index = [ 27,  36], name = "Specimen Plan ID"),
                            dict(index = [ 37,  51], name = "Contract ID"),
                            dict(index = [ 52,  60], name = "Total RESP Assets"),
                            dict(index = [ 61, 500], name = "Filler", isFiller = True)],
                 name = "Summary Report Transaction"),
    '800' : dict(columns = [dict(index = [  1,   3], name = "Record Type"),
                            dict(index = [  4,  11], name = "Transaction Date"),
                            dict(index = [ 12,  26], name = "Promoter Transaction ID"),
                            dict(index = [ 27,  41], name = "Promoter BN"),
                            dict(index = [ 42,  71], name = "Field Name"),
                            dict(index = [ 72,  75], name = "Error Code"),
                            dict(index = [ 76,  76], name = "SIN"),
                            dict(index = [ 77,  77], name = "Given Name"),
                            dict(index = [ 78,  78], name = "Surname"),
                            dict(index = [ 79,  79], name = "Birth Date"),
                            dict(index = [ 80,  80], name = "Sex"),
                            dict(index = [ 81, 500], name = "Filler", isFiller = True)],
                 name = "Transaction Error Report"),
    '850' : dict(columns = [dict(index = [  1,   3], name = "Record Type"),
                            dict(index = [  4,   7], name = "Error Type"),
                            dict(index = [  8, 500], name = "Transaction Data")],
                 name = "Severe Error Report"),
    '900' : dict(columns = [dict(index = [  1,   3], name = "Record Type"),
                            dict(index = [  4,  25], name = "Filler", isFiller = True),
                            dict(index = [ 26,  36], name = "Grant Amount"),
                            dict(index = [ 37,  51], name = "Promoter BN"),
                            dict(index = [ 52,  66], name = "Promoter Transaction ID"),
                            dict(index = [ 67,  67], name = "Refusal Reason"),
                            dict(index = [ 68,  68], name = "Transaction Origin"),
                            dict(index = [ 69,  83], name = "Original Promoter BN"),
                            dict(index = [ 84,  84], name = "Payment Requisitioned"),
                            dict(index = [ 85,  94], name = "Specimen Plan ID"),
                            dict(index = [ 95, 109], name = "Contract ID"),
                            dict(index = [110, 117], name = "CES Program Transaction Date"),
                            dict(index = [118, 126], name = "SIN"),
                            dict(index = [127, 135], name = "CLB Amount"),
                            dict(index = [136, 144], name = "Additional CESG Amount"),
                            dict(index = [145, 153], name = "CLB Fee"),
                            dict(index = [154, 164], name = "Filler", isFiller = True),
                            dict(index = [165, 173], name = "Assisted Contribution Amount"),
                            dict(index = [174, 174], name = "Additional CESG Refusal Reason"),
                            dict(index = [175, 500], name = "Filler", isFiller = True)],
                 name = "Transaction Processing Report"),
    '920' : dict(columns = [dict(index = [  1,   3], name = "Record Type"),
                            dict(index = [  4,  18], name = "Promoter BN"),
                            dict(index = [ 19,  28], name = "Specimen Plan ID"),
                            dict(index = [ 29,  36], name = "Transaction Date"),
                            dict(index = [ 37,  45], name = "Beneficiary SIN"),
                            dict(index = [ 46,  46], name = "SIN Issue"),
                            dict(index = [ 47, 500], name = "Filler", isFiller = True)],
                 name = "SIN Validation Report"),
    '950' : dict(columns = [dict(index = [  1,   3], name = "Record Type"),
                            dict(index = [  4,  18], name = "Promoter BN"),
                            dict(index = [ 19,  28], name = "Specimen Plan ID"),
                            dict(index = [ 29,  43], name = "Contract ID"),
                            dict(index = [ 44,  51], name = "Processing Date"),
                            dict(index = [ 52,  52], name = "Registration Status"),
                            dict(index = [ 53,  53], name = "Reason for Registration Failure"),
                            dict(index = [ 54, 500], name = "Filler", isFiller = True)],
                 name = "Contract Registration Report"),
    '960' : dict(columns = [dict(index = [  1,   3], name = "Record Type"),
                            dict(index = [  4,  11], name = "Received Date"),
                            dict(index = [ 12,  61], name = "Parent Surname"),
                            dict(index = [ 62, 161], name = "Parent Given Name"),
                            dict(index = [162, 171], name = "Postal Code, Zip Code or equivalent"),
                            dict(index = [172, 191], name = "Country"),
                            dict(index = [192, 202], name = "Telephone Number Primary"),
                            dict(index = [203, 203], name = "Telephone Number Primary Teletypewriter (TTY)"),
                            dict(index = [204, 211], name = "Extension Primary"),
                            dict(index = [212, 222], name = "Telephone Number Secondary"),
                            dict(index = [223, 223], name = "Telephone Number Secondary Teletypewriter (TTY)"),
                            dict(index = [224, 231], name = "Extension Secondary"),
                            dict(index = [232, 331], name = "Email Address"),
                            dict(index = [332, 332], name = "Preferred Method of Contact"),
                            dict(index = [333, 333], name = "Preferred Time of Day to be contacted"),
                            dict(index = [334, 334], name = "Preferred Language of Contact"),
                            dict(index = [335, 500], name = "Filler", isFiller = True)],
                 name = "Referral Report"),
    '999' : dict(columns = [dict(index = [  1,   3], name = "Record Type"),
                            dict(index = [  4,  18], name = "Sender BN"),
                            dict(index = [ 19,  26], name = "Date"),
                            dict(index = [ 27,  28], name = "File number"),
                            dict(index = [ 29,  37], name = "Record Count"),
                            dict(index = [ 38, 500], name = "Filler", isFiller = True)],
                 name = "Trailer Record"),
    }

last_record_type=None
try:
    for line in infile:
        record_type = line[0:3]
        if last_record_type != record_type:
            if args.show_section_names or args.show_headers:
                if last_record_type != None: #if not first line ever
                    print("", file=outfile)
            if args.show_section_names:
                print(config[record_type]['name'])
            if args.show_headers:
                # print column names but skip any marked as filler
                # format is: "Column Name (#[col number in current output, fillers considered or filtered], start-end index in the *original* file, useful for tools like cut)
                columns = [col_config for col_config in config[record_type]['columns'] if not (col_config.get('isFiller', False) and args.skip_filler)]
                print(separator.join("{name} (#{colid}, {start}-{end})".format(name = col['name'], colid = idx + 1, start = col['index'][0], end = col['index'][1]) for idx, col in enumerate(columns)))
            last_record_type = record_type

        if record_type in config:
            # printable = not skipped
            printable_indices = [col['index'] for col in config[record_type]['columns'] if not (col.get('isFiller', False) and args.skip_filler)]
            print(separator.join([line[idx[0]-1:idx[1]] for idx in printable_indices]), file=outfile)
        else:
            print(line)

except BrokenPipeError:
    sys.exit()
